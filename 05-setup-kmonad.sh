#!/usr/bin/env bash

KDIR=/home/ulys/OS-setup/utils/kmonad

list_kbds () {
    ls $KDIR/kbds/
}

add_kbd () {
    sed "s|<PATTERN_DEVICE_FILE>|$1|" $KDIR/template.kbd > "$KDIR/kbds/$2.kbd"
}

display_kbds () {
  echo Keyboards with configuration file:
  echo ----------------------------------
  kbds=($(list_kbds))
  for i in $(seq 1 ${#kbds[@]});
     do echo "$i" "${kbds[i-1]/.kbd/}"
  done
  echo "0 - new keyboard -"
}

user_dialog () {
  echo  $1
  echo  -n "> "
  read user_input
}

configure_new_kbd () {
  echo  "Help me found your keyboard:"
  echo  " (1) By path - (mandatory for laptops)"
  echo  " (2) By id"
  user_dialog "Your choice? "
  if [[ "$user_input" == "1" ]]
  then
      dir="path"
  else
      dir="id"
  fi
  kbds=($(ls /dev/input/by-$dir | grep "kbd"))
  for i in $(seq 1 ${#kbds[@]});
     do echo $i ${kbds[i-1]}
  done
  user_dialog "Your choice? "
  kbd_loc=/dev/input/by-$dir/${kbds[user_input-1]}
  echo $kbd_loc
  user_dialog "Give this config a name:"
  add_kbd $kbd_loc $user_input
}

set_config () {
   cp $KDIR/kbds/$1 $KDIR/config.kbd

   sudo groupadd uinput 2> /dev/null
   sudo usermod -aG uinput ulys

   sudo cp $KDIR/99-kmonad.rules /lib/udev/rules.d/

   systemctl --user stop    kmonad.service 2> /dev/null > /dev/null
   systemctl --user disable kmonad.service 2> /dev/null > /dev/null
   systemctl --user enable  kmonad.service 2> /dev/null > /dev/null
   systemctl --user start   kmonad.service 2> /dev/null > /dev/null
}

display_kbds
user_dialog "Your choice?"

if [[ "$user_input" == "0" ]]
then
  configure_new_kbd
  set_config $user_input.kbd
else
  set_config ${kbds[user_input-1]}
fi
